import React from 'react'
import { BottomSheet } from 'react-native-btr';
import { KeyboardAwareScrollView } from "react-native-keyboard-aware-scroll-view"; 
 import {Text,View,ScrollView} from 'react-native'
 const Suma = () =>{
     return(
        <BottomSheet
        visible={this.state.signup_visibility}
        onBackButtonPress={() => this.setState({ signup_visibility: false })}
        onBackdropPress={() => this.setState({ signup_visibility: false ,})}
    >
        {this.state.signup_view_visibility &&
        <View style={{ flex: 1, flexDirection: 'column', justifyContent: 'center' }}>
            <View style={{
                width: '90%',height:'80%',
                flexDirection: 'column', alignSelf: 'center',
                backgroundColor: 'white',
                justifyContent: 'center',
            }}>
                <View style={{
                    height: hp('8%'),
                    alignItems: 'flex-end', justifyContent: 'center'
                }}>
                    <TouchableOpacity
                        onPress={() => {
                            this.setState({ signup_visibility: false })
                        }}                                
                    >
                        <Icon name="close"
                            color={'grey'}
                            style={{ marginRight: 5 }}
                            size={25} />
                    </TouchableOpacity>
                </View>

<KeyboardAwareScrollView
                        contentContainerStyle={{ flexGrow: 1}}
                        showsVerticalScrollIndicator={false}
                        style={{
                            flexDirection: 'column',
                            backgroundColor: '#FFFFFF',
                            
                        }}
                        >
<View style={{flexDirection:'row',backgroundColor:'#FFFFFF',width:'100%',marginTop:'5%'}}>                                    
                        <Image
                    source={logo}
                    style={{ alignSelf: 'center', justifyContent: 'center', padding: 5, width: '25%', height: 60, resizeMode:'contain' }}>
                </Image>
<Text style={{color:'#000000', textAlign:'center',fontSize: 17,padding:10,width:'55%',fontFamily:'OpenSans-Bold',textAlignVertical:'center'}}>Join aidbees</Text>                                                                                                                                     
</View>

<View style={{flexDirection:'column',backgroundColor:'white', paddingTop: 10}}>
{/* Signup name */}
<View style={{ width: '100%',marginTop:'3%'  }}>
                <View style={{
                    width: '90%',  backgroundColor: 'lightgrey',
                    alignSelf: 'center', justifyContent: 'center', borderRadius: 5
                }}>
                    <TextInput style={{ marginLeft: 10, fontSize: 16,fontFamily:'OpenSans', minHeight:54 }} placeholder="Full Name"
                        placeholderTextColor="black" 
                        value={this.state.signup_name}
                        onChangeText={(value) => this.setState({ signup_name : value })}>

                    </TextInput>
                </View>
            </View>
            <View style={{ width: '100%', paddingTop: 10}}>
                <View style={{
                    width: '90%',  backgroundColor: 'lightgrey',
                    alignSelf: 'center', justifyContent: 'center',  borderRadius: 5
                }}>
                    <TextInput style={{ marginLeft: 10, fontSize: 16 ,fontFamily:'OpenSans', minHeight:54}} placeholder="Email"
                        placeholderTextColor="black" 
                        autoCapitalize='none'                                                                
                        value={this.state.email_id}
                        onChangeText={(user_value) => this.email_validation(user_value)}                                   
                        >
                    </TextInput>
                </View>
            </View>
{/* signup password */}
<View style={{ width: '100%', paddingTop: 10, }}>
                <View style={{
                    width: '90%',  backgroundColor: 'lightgrey',
                    alignSelf: 'center',  borderRadius: 5,
                    flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'
                }}>
                    <TextInput
                        style={{ marginLeft: 10, fontSize: 16, width: '80%',fontFamily:'OpenSans', minHeight:54 }}
                        placeholder="Password"
                        placeholderTextColor="black"
                        secureTextEntry={this.state.secureTextEntry}
                        onChangeText={(value) => this.setState({ signup_pwd: value })}
                        value={this.state.signup_pwd}
                    >
                    </TextInput>
                    <TouchableOpacity onPress={() => this.changeIcon()}>
                        <Icon name={this.state.icon} style={{ fontSize: 16, alignSelf: 'center' }} />
                    </TouchableOpacity>
                </View>
            </View>                        
            <View style={{ width: '100%', paddingTop: 10, }}>
                <View style={{
                    width: '90%',  backgroundColor: 'lightgrey',
                    alignSelf: 'center',  borderRadius: 5,
                    flexDirection: 'row', justifyContent: 'flex-start', alignItems: 'center'
                }}>
                    <TextInput
                        style={{ marginLeft: 10, fontSize: 16, width: '80%',fontFamily:'OpenSans', minHeight:54 }}
                        placeholder="Phone Number"
                        maxLength={10}
                        placeholderTextColor="black"
                        keyboardType='numeric'
                        onChangeText={(value) => this.setState({ mobilenum: value })}
                        value={this.state.mobilenum}                                    
                    >
                    </TextInput>                           
                </View>                            
            </View>                        
</View>
<View style={{backgroundColor: 'white', marginTop: 10}}>
<View style={{flexDirection:'row', width:'90%', backgroundColor:'white',marginLeft:15}}>
<View style={{ flexDirection: "row", alignSelf: "center"}}>    
</View>
</View>
<TouchableOpacity onPress={() => this.fun_valid()}>
                    <View style={{
                        width: '40%', height: height / 100 * 7, backgroundColor: '#e0416b',
                        alignSelf: 'center', justifyContent: 'center', marginTop: 20, borderRadius: 10
                    }}>
                        <Text style={{
                            fontSize: 14, color: 'white', fontFamily: 'OpenSans-Bold',
                            alignSelf: 'center', justifyContent: 'center'
                        }}>
                            Sign UP
                    </Text>
                    </View>
                </TouchableOpacity>
                <View style={{flexDirection:'row',alignSelf:'center', marginTop:20 , marginBottom:10, backgroundColor:'white'}} >
<Text style={{fontFamily:'OpenSans'}} >----  Or  ----</Text>
</View>
            <TouchableOpacity  
                onPress={()=>this.signIn()}
                style={{width:'70%',alignSelf:'center',height:40,marginBottom:10 ,}} >
                    <Image
                    source={google}
                    resizeMode='contain'
                    style={{ alignSelf: 'center', justifyContent: 'center', padding: 5, width: '100%', height: 40,}}>
                    </Image>
            </TouchableOpacity>
            {this.isSupported() && (
            <TouchableOpacity  
            onPress={()=>this.appleLogin()}
            style={{width:'70%',alignSelf:'center',height:55,marginBottom:15 ,}} >
                <Image
                source={applesignin}
                resizeMode='contain'
                style={{ alignSelf: 'center', justifyContent: 'center', padding: 5, width: '100%', height: 55, resizeMode:'contain'}}>
                </Image>
        </TouchableOpacity>)}
</View>                           
               </KeyboardAwareScrollView>                            
            </View>
        </View>
}
        
    </BottomSheet>
 
     )
 }