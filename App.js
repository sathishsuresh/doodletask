import React from 'react';
import {View} from 'react-native';

import AppNavigator from './components/Nav';
export default class App extends React.Component {
  render() {
    return <AppNavigator />;
  }
}
