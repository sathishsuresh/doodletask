// import React from 'react'
// import {View,Text} from 'react-native'
// export default class Suma extends React.Component{
//     render(){
//         return(
//             <View>
//                 <Text>hu</Text>
//             </View>
//         )
//     }
// }
//App.js
import React, {Component} from 'react';
import {
  StyleSheet,
  Text,
  View,
  ActivityIndicator,
  FlatList,
} from 'react-native';
import {TouchableOpacity} from 'react-native-gesture-handler';
import Euro from 'react-native-vector-icons/MaterialIcons';
import Leftarrow from 'react-native-vector-icons/AntDesign';
import Upload from 'react-native-vector-icons/Feather';
import Info from 'react-native-vector-icons/Feather';
import Plus from 'react-native-vector-icons/AntDesign';
import Minus from 'react-native-vector-icons/AntDesign';
import Chat from 'react-native-vector-icons/Entypo';
import Localdining from 'react-native-vector-icons/MaterialIcons';
import Delivery from 'react-native-vector-icons/MaterialCommunityIcons';

// import RadioForm, {RadioButton, RadioButtonInput, RadioButtonLabel} from 'react-native-simple-radio-button';
import Shoppingcart from 'react-native-vector-icons/AntDesign';
import { RadioButton } from 'react-native-paper';
class App extends Component {
  constructor(props){
    super(props)
    this.state = {
      counter: 0,
      Add_visibility: false,
      Counter_Visibility: true,
      cart_total: 0,
      total_amount: 0,
      loadMoreVisible:true,
      // sample_data: [
      //   {
      //     type1: 'N',
      //     type2: 'D',
      //     dish: 'Guaca de la Costa',
      //     restuarant: 'res1',
      //     amount: '67',
      //     counter: 0,
      //     isSelected: 0,
      //   },
      //   {
      //     type1: 'N',
      //     type2: 'D',
      //     dish: 'Chicharron y Cerveza',
      //     restuarant: 'res1',
      //     amount: '67',
      //     counter: 0,
      //     isSelected: 0,
      //   },
      //   {
      //     type1: 'N',
      //     type2: 'D',
      //     dish: 'Chilitos Con Carrot',
      //     restuarant: 'res1',
      //     amount: '67',
      //     counter: 0,
      //     isSelected: 0,
      //   },
      //   {
      //     type1: 'N',
      //     type2: 'D',
      //     dish: 'Dosa',
      //     restuarant: 'res1',
      //     amount: '67',
      //     counter: 0,
      //     isSelected: 0,
      //   },
      //   {
      //     type1: 'N',
      //     type2: 'D',
      //     dish: 'Idly',
      //     restuarant: 'res1',
      //     amount: '67',
      //     counter: 0,
      //     isSelected: 0,
      //   },
      //   {
      //     type1: 'N',
      //     type2: 'D',
      //     dish: 'Rice',
      //     restuarant: 'res1',
      //     amount: '67',
      //     counter: 0,
      //     isSelected: 0,
      //   },
      //   {
      //     type1: 'N',
      //     type2: 'D',
      //     dish: 'Rice',
      //     restuarant: 'res1',
      //     amount: '67',
      //     counter: 0,
      //     isSelected: 0,
      //   },
      //   {
      //     type1: 'N',
      //     type2: 'D',
      //     dish: 'Rice',
      //     restuarant: 'res1',
      //     amount: '67',
      //     counter: 0,
      //     isSelected: 0,
      //   },
      // ],
      sample_data:[],
       radio_props : [
        {label: 'param1', value: 0 },
        {label: 'param2', value: 1 } 
],
      otheruser_id: 0,
      page:1,
      perPage:1,
      displayArray:[],
      checked:'first',
    };
  
  }
 
  count = (type, index) => {
    const {counter, sample_data, cart_total} = this.state;

    let selectedArray = sample_data[index];

    if (type == 'increment' && selectedArray.counter < 20) {
      selectedArray.counter = selectedArray.counter + 1;
      selectedArray.isSelected = !selectedArray.isSelected;
      sample_data[index] = selectedArray;
      this.setState({sample_data: sample_data});
     
      //   for(let i=0; i<=sample_data.length; i++){
      //     sample_data.isSelected = sample_data.isSelected + sample_data[i].counter
      //     }
      this.setState({cart_total: cart_total});
    } else if (type == 'decrement' && selectedArray.counter > 0) {
      selectedArray.counter = selectedArray.counter - 1;
      selectedArray.isSelected = !selectedArray.isSelected;
      sample_data[index] = selectedArray;
      this.setState({sample_data: sample_data});

      if (selectedArray.counter == 0) {
        //  this.state.Add_visibility=true
        //  this.state.Counter_Visibility=false
      }
    }
  };

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '100%',
          backgroundColor: '#000',
        }}
      />
    );
  };

  componentDidMount() {
    
    const {navigation,sample_data} = this.props;
    
   let cart_items =  navigation.getParam('cart_items')
   let total_Amount = navigation.getParam('totalAmount')
   
    console.log('data-->',JSON.stringify(cart_items) + total_Amount)
    // this.setState({sample_data: cart_items})
    this.state.sample_data = cart_items
    this.setState({total_amount: total_Amount})
    console.log('item data-->',this.state.sample_data)
    this.setNewData()

  }
  setNewData(){ 
    
    var tempArray=[]
    if(this.state.sample_data.length == this.state.displayArray.length){
      this.setState({
        loadMoreVisible:false
      })
    }else{
       for(var i=0; i<(this.state.page*this.state.perPage); i++){
      tempArray.push(this.state.sample_data[i])
      }
      this.setState({
        displayArray: tempArray,
        loadMoreVisible:true
      })
    }

   
  }
  loadMore(){
    this.setState({
      page: this.state.page+1
    },()=>{
      this.setNewData()
    })
    //alert('load')
  }
  

  render() {
    const{checked}=this.state
    return (
      <View style={{flex: 1, width: '100%', height: '100%'}}>
        <View
          style={{
            height: '35%',
            width: '100%',
            backgroundColor: '#0A1E2E',
            flexDirection: 'column',
          }}>
          <View
            style={{
              height: '20%',
              width: '100%',
              backgroundColor: '#0A1E2E',
              flexDirection: 'row',
              marginTop: '4%',
            }}>
            <TouchableOpacity
              onPress={() => this.props.navigation.navigate('Main')}>
              <Leftarrow
                name="arrowleft"
                size={30}
                color="white"
                style={{marginTop: 10, marginLeft: 5}}
              />
            </TouchableOpacity>
            <Text
              style={{
                fontWeight: 'bold',
                color: 'white',
                fontSize: 18,
                marginStart: 15,
                marginTop: 10,
              }}>
              My Cart
            </Text>
          </View>

          <View
            style={{
              height: '35%',
              width: '40%',
              backgroundColor: 'white',
              borderRadius: 6,
              borderWidth: 2,
              flexDirection: 'column',
              alignItems: 'center',
              alignSelf: 'center',
              justifyContent: 'center',
              marginTop: '5%',
            }}>
            <Text style={{color: '#DBC7B2'}}>Total Cost</Text>
            <View style={{flexDirection: 'row'}}>
              <Euro name="euro" size={15} color="gray" style={{marginTop: 3}} />
              <Text style={{color: '#000000'}}> {this.state.total_amount}</Text>
            </View>
          </View>
        </View>

        <View
          style={{
            height: '100%',
            width: '100%',
            backgroundColor: 'white',
            flexDirection: 'column',
          }}>
          <Text style={{color: '#000000', padding: 10}}>Review Orders</Text>

          <View style={{ flex: 0.6,paddingBottom:10}}>
            <FlatList
              data={this.state.displayArray}
              scrollEnabled={true}
              renderItem={({item, index}) => (
                <View style={{flexDirection: 'row', flex: 1}}>
                  <View
                    style={{
                      flexDirection: 'row',
                      width: '100%',
                      paddingBottom: '5%',
                      paddingTop: '5%',
                      marginLeft: 10,
                    }}>
                    <View style={{flexDirection: 'column', width: '10%'}}>
                      <View
                        style={{
                          borderRadius: 6,
                          borderWidth: 0.5,
                          width: 30,
                          height: 30,
                          justifyContent: 'center',
                          alignItems: 'center',
                        }}>
                        <Text>{item.type1}</Text>
                      </View>
                      <View
                        style={{
                          borderRadius: 6,
                          borderWidth: 0.5,
                          width: 30,
                          height: 30,
                          justifyContent: 'center',
                          alignItems: 'center',
                          marginTop: 5,
                        }}>
                        <Text>{item.type2}</Text>
                      </View>
                    </View>

                    <View style={{flexDirection: 'column', width: '55%'}}>
                      <Text style={{marginTop: 5, marginStart: 5}}>
                        {item.dish}
                      </Text>
                      <Text style={{marginTop: 5, marginStart: 5}}>
                        {item.restuarant}
                      </Text>
                      <View style={{flexDirection: 'row', marginTop: 5}}>
                        <Euro
                          name="euro"
                          size={15}
                          color="gray"
                          style={{marginTop: 3}}
                        />
                        <Text style={{marginStart: 2}}>{item.amount}</Text>
                      </View>
                    </View>

                    <View style={{flexDirection: 'column', width: '50%'}}>
                      {this.state.Add_visibility && (
                        <View
                          style={{
                            width: '60%',
                            height: 30,
                            backgroundColor: '#0A1E2E',
                            borderRadius: 5,
                          }}>
                          <TouchableOpacity
                            onPress={() =>
                              this.setState({
                                Counter_Visibility: true,
                                Add_visibility: false,
                              })
                            }>
                            <Text
                              style={{
                                alignSelf: 'center',
                                marginTop: 3,
                                color: 'white',
                              }}>
                              add
                            </Text>
                          </TouchableOpacity>
                        </View>
                      )}

                      {this.state.Counter_Visibility && (
                        <View
                          style={{
                            width: '50%',
                            height: 30,
                            flexDirection: 'row',
                            borderWidth: 1,
                            borderColor: 'red',
                          }}>
                          <TouchableOpacity
                            onPress={() => this.count('decrement', index)}
                            style={{width: '100%'}}>
                            <Minus
                              name="minus"
                              size={20}
                              color="#000"
                              style={{
                                marginTop: 3,
                                alignSelf: 'center',
                                justifyContent: 'center',
                              }}
                            />
                          </TouchableOpacity>
                          <View style={{width: '35%'}}>
                            <Text
                              style={{
                                alignSelf: 'center',
                                justifyContent: 'center',
                                marginTop: 3,
                              }}>
                              {item.counter}
                            </Text>
                          </View>
                          <TouchableOpacity
                            onPress={() => this.count('increment', index)}
                            style={{width: '100%'}}>
                            <Plus
                              name="plus"
                              size={20}
                              color="#000"
                              style={{
                                marginTop: 3,
                                alignSelf: 'center',
                                justifyContent: 'center',
                              }}
                            />
                          </TouchableOpacity>
                        </View>
                      )}
                      {/* <Text>hi</Text> */}
                      <View style={{width: '30%', marginTop: 5}}>
                        <Chat
                          name="chat"
                          size={20}
                          color="#000"
                          style={{
                            marginTop: 5,
                            alignSelf: 'center',
                            justifyContent: 'center',
                          }}
                        />
                      </View>
                    </View>
                  </View>
                </View>
              )}
              ItemSeparatorComponent={this.renderSeparator}
            />
            {this.state.loadMoreVisible == true?

      
<View style={{width:'100%', height:20, justifyContent:'flex-end', alignItems:'flex-end',marginEnd:10}} >
  <TouchableOpacity onPress={()=>this.loadMore()} >
  <Text style={{marginEnd:20}} >
  Load more
  </Text>
  </TouchableOpacity>
  </View>:null
}
          </View>
          <Text>Delivery Option</Text>
          <View style={{flexDirection:'row'}} >
            
          <View style={{flexDirection:'row', width:'50%',alignSelf:'center'}}>
          <Localdining
                          name="local-dining"
                          size={20}
                          color="#000"
                          style={{marginTop:5,marginStart:5}}
                          
                        />
  
  <Text style={{width:'30%',marginStart:10,marginTop:5}}>Dine-In</Text>

  <RadioButton
        value="first"
        status={ checked === 'first' ? 'checked' : 'unchecked' }
        onPress={() => this.setState({checked:'first'})}
        color={'orange'}
        uncheckedColor={'grey'}
      />
      </View>
      <View style={{flexDirection:'row', width:'50%'}}>
      <Delivery
                          name="truck-delivery"
                          size={20}
                          color="#000"
                          style={{marginTop:5}}
                          
                        />
      <Text style={{width:'40%',marginStart:10,marginTop:5}}>Take Way</Text>
      <RadioButton
        value="second"
        status={ checked === 'second' ? 'checked' : 'unchecked' }
        onPress={() => this.setState({checked:'second'})}
        color={'orange'}
        uncheckedColor={'grey'}
      />
      </View>
      </View>
      <View style={{width:'100%',height:60,backgroundColor: '#0A1E2E'}} >
        <Text style={{color:'white',alignSelf:'center',justifyContent:'center',marginTop:3}} >Place Order</Text>
      </View>
          {/* <View style={{flexDirection:'row',width:'100%'}} >
              
              <View style={{width:'50%',marginStart:10,flexDirection:'row'}} >
               
              <Localdining
                          name="local-dining"
                          size={20}
                          color="#000"
                          
                        />
                         <Text style={{marginStart:10,marginEnd:10}} > Dine-in </Text>
                        <RadioForm
          radio_props={this.state.radio_props}
          style={{marginStart:10}}
          labelHorizontal={true}
          formHorizontal={true}
          onPress={(value) => {this.setState({value:value})}}
        />
              </View>
          </View> */}
          {/* <RadioForm
          radio_props={this.state.radio_props}
          initial={0}
          labelHorizontal={true}
          formHorizontal={true}
          onPress={(value) => {this.setState({value:value})}}
        /> */}
        {/* <RadioForm
          radio_props={this.state.radio_props}
          initial={0}
          formHorizontal={true}
          onPress={(value) => {this.setState({value:value})}}
          buttonColor={'grey'}
          selectedButtonColor={'orange'}
               /> */}
          <TouchableOpacity
            style={{backgroundColor: '#0A1E2E', width: '100%', height: 32,}}>
            <View>
              <Text style={{alignSelf: 'center', color: 'white', fontSize: 15,alignSelf:'center',justifyContent:'center'}}>
                Place Order
              </Text>
            </View>
          </TouchableOpacity>
          
          {/* <TouchableOpacity 
// onPress={()=>this.props.navigation.navigate('Cart')}
style={{width:'100%',height:50,backgroundColor:'#0A1E2E',alignSelf:'center',justifyContent:'center',flexDirection:'row',marginTop:'20%'}}>
<View  >

<View style={{flexDirection:'row',alignSelf:'center',justifyContent:'center'}} >
<Shoppingcart name="shoppingcart" size={20} color="white" style={{marginTop:3,alignSelf:'center',justifyContent:'center'}} />
  <Text style={{color:'white',alignSelf:'center',marginStart:8}} >VIEW CART </Text>
  <Text style={{color:'white',alignSelf:'center',marginStart:3}} >( {this.state.cart_total} ITEMS) </Text>
  </View>
</View>
</TouchableOpacity> */}
        </View>
      </View>
    );
  }
}

export default App;
