import React from 'react';
import {createAppContainer, createSwitchNavigator} from 'react-navigation';
import {createStackNavigator} from 'react-navigation-stack';
import Main from './Mainscreen';
import Cart from './Cart';

const Mainstack = createStackNavigator({
  Main: {
    screen: Main,
    navigationOptions: {
      headerShown: false,
    },
  },
  Cart: {
    screen: Cart,
    navigationOptions: {
      headerShown: false,
    },
  },
});

const AppNavigator = createSwitchNavigator(
  {
    Mainstack: Mainstack,
  },
  {
    initialRouteName: 'Mainstack',
  },
);

export default createAppContainer(AppNavigator);
