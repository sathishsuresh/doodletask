import React from 'react';
import {
  Text,
  View,
  SafeAreaView,
  Image,
  ImageBackground,
  FlatList,
  TouchableOpacity,
  ScrollView,
} from 'react-native';
import Telephone from 'react-native-vector-icons/Foundation';
import Star from 'react-native-vector-icons/FontAwesome';
import Leftarrow from 'react-native-vector-icons/AntDesign';
import Upload from 'react-native-vector-icons/Feather';
import Info from 'react-native-vector-icons/Feather';
import Plus from 'react-native-vector-icons/AntDesign';
import Minus from 'react-native-vector-icons/AntDesign';
import Euro from 'react-native-vector-icons/MaterialIcons';
import Shoppingcart from 'react-native-vector-icons/AntDesign';
import Localdining from 'react-native-vector-icons/MaterialIcons';
export default class Mainscreen extends React.Component {
  constructor(props){
    super(props);
    this.state = {
      counter: 0,
      //  Add_visibility:true,
      Counter_Visibility: false,
      cart_total: 0,
      total_amount: 0,
      sample_data: [
        {
          type1: 'S',
          type2: 'D',
          dish: 'Guaca de la Costa',
          restuarant: 'res1',
          amount: 67,
          counter: 0,
          isSelected: false,
          id: 0,
        },
        {
          type1: 'a',
          type2: 'D',
          dish: 'Chicharron y Cerveza',
          restuarant: 'res1',
          amount: 50,
          counter: 0,
          isSelected: false,
          id: 1,
        },
        {
          type1: 't',
          type2: 'D',
          dish: 'Chilitos Con Carrot',
          restuarant: 'res1',
          amount: 23,
          counter: 0,
          isSelected: false,
          id: 2,
        },
        // {
        //   type1: 'N',
        //   type2: 'D',
        //   dish: 'Dosa',
        //   restuarant: 'res1',
        //   amount: '67',
        //   counter: 0,
        //   isSelected: false,
        //   id: 3,
        // },
        // {
        //   type1: 'N',
        //   type2: 'D',
        //   dish: 'Idly',
        //   restuarant: 'res1',
        //   amount: '67',
        //   counter: 0,
        //   isSelected: false,
        //   id: 4,
        // },
        // {
        //   type1: 'N',
        //   type2: 'D',
        //   dish: 'Rice',
        //   restuarant: 'res1',
        //   amount: '67',
        //   counter: 0,
        //   isSelected: false,
        //   id: 5,
        // },
        // {
        //   type1: 'N',
        //   type2: 'D',
        //   dish: 'Rice',
        //   restuarant: 'res1',
        //   amount: '67',
        //   counter: 0,
        //   isSelected: false,
        //   id: 6,
        // },
        // {
        //   type1: 'N',
        //   type2: 'D',
        //   dish: 'Rice',
        //   restuarant: 'res1',
        //   amount: '67',
        //   counter: 0,
        //   isSelected: false,
        //   id: 7,
        // },
      ],
      selected_items :[],
      newArray: [],
    };
  }

  count = (type, index, item) => {
    console.log('type', type + 'item' + item);
    const {counter, sample_data, cart_total,selected_items,newArray} = this.state;

    let selectedArray = sample_data[index];


    
    if (type == 'Visibility') {
      console.log(type);
      selectedArray.isSelected = !selectedArray.isSelected;

      sample_data[index] = selectedArray;
      
    }
    this.setState({sample_data: sample_data});



    if (type == 'increment' && selectedArray.counter < 20) {
      selectedArray.counter = selectedArray.counter + 1;
      sample_data[index] = selectedArray;
      this.setState({sample_data: sample_data});

      console.log('Sample Data --> whole data', JSON.stringify(sample_data));


     this.setState({newArray: sample_data[index]})  


    console.log('selected Items Array' + JSON.stringify(newArray))
      if(newArray.id == sample_data[index].id){
        selected_items[index] = sample_data[index]
      }
     else{
      selected_item_add = selected_items.concat(sample_data[index])
      this.setState({selected_items: selected_item_add})
     }
     
     console.log('Selected Data -->', JSON.stringify(selected_items));

      this.state.total_amount = this.state.total_amount + sample_data[index].amount
      console.log('Total Ammnt',+ this.state.total_amount )
   

      let totalquantity = 0;
      sample_data.forEach(item => {
        totalquantity = totalquantity + item.counter;
      });
      console.log('plus', totalquantity);
      console.log('itemcounter', item.counter);
    
      this.setState({cart_total: totalquantity});
    } 
    
    else if (type == 'decrement' && selectedArray.counter > 0) {
      console.log('selec counter' , selectedArray.counter)
      selectedArray.counter = selectedArray.counter - 1;
      sample_data[index] = selectedArray;
      this.setState({sample_data: sample_data});

      console.log('Sample Data --> whole data', JSON.stringify(sample_data));


     this.setState({newArray: sample_data[index]})  


    console.log('selected Items Array' + JSON.stringify(newArray))
      if(newArray.id == sample_data[index].id){
        // console.log('counterrrrrrrrr ---------',selected_items[index].counter )
        if(selected_items[index] > 0){
          selected_items[index] = sample_data[index]
        }
        else {
          selected_item_add = selected_items.splice(sample_data[index],1)
          this.setState({selected_items: selected_item_add})
        }
        console.log('splice---', selected_item_add)

    

      }
      //this.setState({selected_items: selected_item_add})
     
     console.log('Selected Data -->', JSON.stringify(selected_items));

      this.state.total_amount = this.state.total_amount - sample_data[index].amount
      console.log('Total Ammnt',+ this.state.total_amount )
   

      let totalquantity = 0;
      sample_data.forEach(item => {
        console.log('minus ----', totalquantity);
        console.log('itemcounter ----', item.counter);
        totalquantity = totalquantity + item.counter;
      });
      console.log('minus', totalquantity);
      console.log('itemcounter', item.counter);
    
      this.setState({cart_total: totalquantity});
      if (selectedArray.counter == 0) {
        console.log('jghj', selectedArray.id + item.id);
        selectedArray.isSelected = !selectedArray.isSelected;
      }
    }
    
    
  
  };

  renderSeparator = () => {
    return (
      <View
        style={{
          height: 1,
          width: '100%',
          backgroundColor: '#000',
        }}
      />
    );
  };

  render() {
    const {sample_data} = this.state;

    return (
      <SafeAreaView
        style={{
          flex: 1,
          width: '100%',
          height: '100%',
          backgroundColor: 'white',
        }}>
        <View style={{width: '100%', height: '100%'}}>
          <View>
            <View style={{width: '100%', height: '50%'}}>
              <ImageBackground
                source={require('../assets/image_food.jpg')}
                style={{width: '100%', height: '100%', resizeMode: 'cover'}}>
                <View
                  style={{flexDirection: 'row', width: '100%', marginTop: 10}}>
                  <View style={{width: '20%'}}>
                    <Leftarrow
                      name="arrowleft"
                      size={30}
                      color="white"
                      style={{marginTop: 10, marginLeft: 5}}
                    />
                  </View>
                  <View style={{width: '50%'}}></View>
                  <View style={{width: '15%'}}>
                    <Upload
                      name="upload"
                      size={30}
                      color="white"
                      style={{marginTop: 10, marginLeft: 5}}
                    />
                  </View>
                  <View style={{width: '15%'}}>
                    <Info
                      name="info"
                      size={30}
                      color="white"
                      style={{marginTop: 10, marginLeft: 5}}
                    />
                  </View>
                </View>
              </ImageBackground>
            </View>
            <View
              style={{
                alignSelf: 'center',
                width: '80%',
                height: '60%',
                backgroundColor: 'white',
                position: 'absolute',
                marginTop: '20%',
                elevation: 5,
              }}>
              <View style={{alignSelf: 'center'}}>
                <Text
                  style={{marginTop: 20, fontSize: 20, alignSelf: 'center'}}>
                  Inka Restaurant
                </Text>
                <View style={{flexDirection: 'row', marginTop: 10}}>
                  <Star
                    name="star-o"
                    size={15}
                    color="gray"
                    style={{marginTop: 3}}
                  />
                  <Text style={{marginLeft: 2}}>
                    5.0(200+)| All days : 09:00 AM - 06:00 PM
                  </Text>
                </View>
                <View
                  style={{
                    flexDirection: 'row',
                    marginTop: 10,
                    alignSelf: 'center',
                  }}>
                  <Telephone
                    name="telephone"
                    size={15}
                    color="gray"
                    style={{marginTop: 3}}
                  />
                  <Text style={{marginLeft: 10}}>Reach us at : 9854562142</Text>
                </View>
                <View
                  style={{
                    height: '30%',
                    backgroundColor: '#0A1D2F',
                    alignSelf: 'center',
                    borderRadius: 20,
                    width: 150,
                    marginTop: 10,
                  }}>
                  <Text
                    style={{
                      alignSelf: 'center',
                      marginTop: 8,
                      fontSize: 20,
                      color: 'white',
                    }}>
                    Book a table
                  </Text>
                </View>
              </View>
            </View>
          </View>
          <View
            style={{
              width: '100%',
              backgroundColor: 'white',
              flexDirection: 'column',
            }}>
            <Text style={{color: '#000000', padding: 10,fontSize:15}}>Starter</Text>
         
          </View>
          <View style={{flex:1,flexDirection:'column',width:'100%', backgroundColor:'white'}}>
          <FlatList
                data={this.state.sample_data}
                scrollEnabled={true}
                renderItem={({item, index}) => (
                  <View style={{flexDirection: 'row', flex: 1,alignSelf:'center',alignItems:'center',}}>
                    <View
                      style={{
                        flexDirection: 'row',
                        width: '100%',
                        paddingBottom: '5%',
                        paddingTop: '5%',
                        marginLeft: 10,
                      }}>
                      <View style={{flexDirection: 'column', width: '10%'}}>
                        <View
                          style={{
                            borderRadius: 6,
                            borderWidth: 0.5,
                            width: 30,
                            height: 30,
                            justifyContent: 'center',
                            alignItems: 'center',
                          }}>
                          <Text>{item.type1}</Text>
                        </View>
                        <View
                          style={{
                            borderRadius: 6,
                            borderWidth: 0.5,
                            width: 30,
                            height: 30,
                            justifyContent: 'center',
                            alignItems: 'center',
                            marginTop: 5,
                          }}>
                          <Text>{item.type2}</Text>
                        </View>
                      </View>

                      <View style={{flexDirection: 'column', width: '55%'}}>
                        <Text style={{marginTop: 5, marginStart: 5}}>
                          {item.dish}
                        </Text>
                        <Text style={{marginTop: 5, marginStart: 5}}>
                          {item.restuarant}
                        </Text>
                        <View style={{flexDirection: 'row', marginTop: 5}}>
                          <Euro
                            name="euro"
                            size={15}
                            color="gray"
                            style={{marginTop: 3}}
                          />
                          <Text style={{marginStart: 2}}>{item.amount}</Text>
                        </View>
                      </View>

                      <View style={{flexDirection: 'column', width: '50%'}}>
                        {item.isSelected == false ? (
                          <View
                            style={{
                              width: '60%',
                              height: 30,
                              backgroundColor: '#0A1E2E',
                              borderRadius: 5,
                            }}>
                            <TouchableOpacity
                              onPress={() =>
                                this.count('Visibility', index, item)
                              }>
                              <Text
                                style={{
                                  alignSelf: 'center',
                                  marginTop: 3,
                                  color: 'white',
                                }}>
                                add
                              </Text>
                            </TouchableOpacity>
                          </View>
                        ) : (
                          <View
                            style={{
                              width: '60%',
                              height: 30,
                              flexDirection: 'row',
                              borderWidth: 1,
                              borderColor: 'red',
                            }}>
                            <TouchableOpacity
                              onPress={() =>
                                this.count('decrement', index, item)
                              }
                              style={{width: '30%'}}>
                              <Minus
                                name="minus"
                                size={20}
                                color="#000"
                                style={{
                                  marginTop: 3,
                                  alignSelf: 'center',
                                  justifyContent: 'center',
                                }}
                              />
                            </TouchableOpacity>
                            <View style={{width: '35%'}}>
                              <Text
                                style={{
                                  alignSelf: 'center',
                                  justifyContent: 'center',
                                  marginTop: 3,
                                }}>
                                {item.counter}
                              </Text>
                            </View>
                            <TouchableOpacity
                              onPress={() =>
                                this.count('increment', index, item)
                              }
                              style={{width: '35%'}}>
                              <Plus
                                name="plus"
                                size={20}
                                color="#000"
                                style={{
                                  marginTop: 3,
                                  alignSelf: 'center',
                                  justifyContent: 'center',
                                }}
                              />
                            </TouchableOpacity>
                          </View>
                        )}

                        {/* {   this.state.Counter_Visibility &&
                      <View style={{width:'60%',height:30,flexDirection:'row',borderWidth:1,borderColor:'red'}} >
                        <TouchableOpacity  
                         onPress={()=>this.count("decrement", index,item)}
                        style={{width:'30%'}}>
                        
                        <Minus name="minus" size={20} color="#000" style={{marginTop:3,alignSelf:'center',justifyContent:'center'}} />
                       
                        </TouchableOpacity>
                        <View style={{width:'35%'}} >
                        <Text style={{alignSelf:'center',justifyContent:'center',marginTop:3}} >{item.counter}</Text>
                        </View>  
                        <TouchableOpacity 
                        onPress={()=>this.count("increment", index,item)}
                        style={{width:'35%'}} >
                       
                        <Plus name="plus" size={20} color="#000" style={{marginTop:3,alignSelf:'center',justifyContent:'center'}} />
                      
                        </TouchableOpacity>                     
                      </View>
             
             } */}
                        {/* <Text>hi</Text> */}
                        
                      </View>
                    </View>
                    
                  </View>
                  
                )}
                ItemSeparatorComponent={this.renderSeparator}
              />
          </View>

<View>
     
<TouchableOpacity
              onPress={() => this.props.navigation.navigate('Cart',{cart_items:this.state.selected_items, totalAmount: this.state.total_amount})}
              style={{
                width: '100%',
                backgroundColor: '#0A1E2E',
                alignSelf: 'center',
                justifyContent: 'center',
                flexDirection: 'row',
              }}>
                <View style={{width:'20%',backgroundColor:'orange',position:'absolute',marginTop:-30,borderRadius:5,flexDirection:'row'}} >
                <Localdining
                          name="local-dining"
                          size={20}
                          color="#000"
                          style={{marginTop:5,marginStart:5}}
                          
                        />
                  <Text style={{marginTop:3}} > MENU </Text>
                </View>

<View
                  style={{
                    flexDirection: 'row',
                    alignSelf: 'center',
                    justifyContent: 'center',
                    marginTop: 8,
                    height:50
                  }}>
                  <Shoppingcart
                    name="shoppingcart"
                    size={20}
                    color="white"
                    style={{
                      marginTop: 3,
                      alignSelf: 'center',
                      justifyContent: 'center',
                    }}
                  />
                  <Text
                    style={{
                      color: 'white',
                      alignSelf: 'center',
                      marginStart: 8,
                    }}>
                    VIEW CART{' '}
                  </Text>
                  <Text
                    style={{
                      color: 'white',
                      alignSelf: 'center',
                      marginStart: 3,
                    }}>
                    ( {this.state.cart_total} ITEMS){' '}
                  </Text>
                </View>
          
          </TouchableOpacity>
             </View>
          
        </View>
      </SafeAreaView>
    );
  }
}
